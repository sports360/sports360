package com.sports360.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sports360.entities.LoginModel;
import com.sports360.entities.User;
import com.sports360.entities.UserModel;
import com.sports360.services.UserService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/user")
public class UserController {
	@Autowired
	private UserService userService;

	ObjectMapper mapper = new ObjectMapper();

	@PostMapping("/registration")
	public ResponseEntity<?> userRegistration(@RequestBody User user) {
		int status = userService.doUserRegister(user);
		switch (status) {
		case -1:
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"email already exist\",\"statuscode\":" + 202 + "}");
		case -2:
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Registration success\",\"statuscode\":" + 200 + "}");
		default:
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Registration failed\",\"statuscode\":" + 201 + "}");
		}

	}

	/**
	 * Controller method for login functionality
	 * 
	 * @param emailId
	 * @param password
	 * @return if username and password matches it returns true, else return false
	 */
	@PostMapping("/login")
	public ResponseEntity<?> login(@RequestBody LoginModel loginModel) {

		User user = userService.doLogin(loginModel);
		if (user != null) {
			return ResponseEntity.status(HttpStatus.OK).body("{\"status\":\"Login success\",\"statuscode\":" + 200
					+ ",\"userId\":" + user.getUserId() + ",\"role\":\"" + user.getRole() + "\"}");
		} else {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid credentials\",\"statuscode\":" + 201 + "}");
		}
	}
/**
 * Controller method for search the user by using emailId
 * @param emailId
 * @return user object, if the user with corresponding emailId found, else return empty object
 */
	@GetMapping("/serachbyemailid/{emailId}")
	public ResponseEntity<?> serachByEmailId(@PathVariable("emailId") String emailId) {
		UserModel user = userService.searchByEmailId(emailId);

		String JsonObject = null;
		if (user != null) {

			try {
				JsonObject = mapper.writeValueAsString(user);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"User Details\":" + JsonObject + ",\"statuscode\":" + 201 + "}");

		} else {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"EmailId doesn't exist\",\"statuscode\":" + 201 + "}");

		}

	}
}
