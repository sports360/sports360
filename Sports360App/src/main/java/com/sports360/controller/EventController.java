package com.sports360.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sports360.entities.Event;
import com.sports360.entities.EventModel;
import com.sports360.services.EventService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/event")
public class EventController {

	@Autowired
	private EventService eventService;

	@Autowired
	private ObjectMapper mapper;

	/**
	 * controller method for create a event
	 * 
	 * @param event
	 * @return 200 if event creation done, else 201
	 */
	@PostMapping("/creation")
	public ResponseEntity<?> eventCreation(@RequestBody Event event) {
		boolean eventCreationStatus = eventService.eventCreation(event);
		if (eventCreationStatus) {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Event Created\",\"statuscode\":" + 200 + "}");
		} else {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Event Creation failed\",\"statuscode\":" + 201 + "}");
		}
	}

	/**
	 * This web resource for getting all events.
	 * 
	 * @return List of events
	 */
	@GetMapping("/getallevents")
	public ResponseEntity<?> getAllEvents() {
		List<EventModel> events = eventService.getAllEvents();
		String eventList = null;
		try {
			eventList = mapper.writeValueAsString(events);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		if (events.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"No Events found\",\"statuscode\":" + 200 + "}");
		} else {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"eventlist\":" + eventList + ",\"statuscode\":" + 201 + "}");
		}
	}

	@GetMapping("/getallupcomingevents")
	public ResponseEntity<?> getAllUpcomingEvents() {
		return null;
	}

	@GetMapping("/getalleventsondate/{date}")
	public ResponseEntity<?> getAllEventsOnDate(
			@PathVariable("date") @DateTimeFormat(pattern = "yyyy-MM-dd") Date date) {
		List<EventModel> events = eventService.getAllEventsOnDate(date);
		String eventlist = null;
		try {
			eventlist = mapper.writeValueAsString(events);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		if (events.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"No Events found\",\"statuscode\":" + 200 + "}");
		} else {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"eventlist\":" + eventlist + ",\"statuscode\":" + 201 + "}");
		}
	}

	/**
	 * Controller method for search the events bases on the location
	 * 
	 * @param location
	 * @return if events found return list of events with 200 status code, else
	 *         return 201 status code
	 */
	@GetMapping("/searcheventbylocation/{search_location}")
	public ResponseEntity<?> searchEventByLocation(@PathVariable("search_location") String search_location) {
		System.out.println(search_location);
		List<EventModel> eventList = eventService.searchEventByLocation(search_location);
		Map<String, Object> eventMap = new HashMap<>();

		String entityJson = null;
		if (eventList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"No Events found\",\"statuscode\":" + 201 + "}");
		} else {

			eventMap.put("Status", "Events found");
			eventMap.put("Status Code", 200);
			eventMap.put("Events", eventList);
			// TODO: remove this line
			try {
				// converting java object into JSON format
				entityJson = mapper.writeValueAsString(eventMap);
			} catch (JsonProcessingException e) {
				e.printStackTrace();

			}
			return ResponseEntity.status(HttpStatus.OK).body(entityJson);
		}

	}

	@PutMapping("/modifyevent")
	public ResponseEntity<?> modifyEvent(@RequestBody Event event) {
		boolean isModified = eventService.doModifyEvent(event);
		if (isModified) {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Event Modified successfully\",\"statuscode\":" + 200 + "}");
		} else {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Event Modification failed\",\"statuscode\":" + 201 + "}");
		}
	}

	@GetMapping("/geteventbyid/{eventId}")
	public ResponseEntity<?> getEventById(@PathVariable("eventId") int eventId) {
		Event event = eventService.getEventById(eventId);
		Map<String, Object> responseMap = new HashMap<>();

		if (event != null) {
			responseMap.put("statuscode", 200);
			responseMap.put("result", event);
			String jsonResponse = null;
			try {
				jsonResponse = mapper.writeValueAsString(responseMap);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
			return ResponseEntity.status(HttpStatus.OK).body(jsonResponse);
		} else {
			responseMap.put("statuscode", 201);
			responseMap.put("status", "no event found");
			String jsonResponse = null;
			try {
				jsonResponse = mapper.writeValueAsString(responseMap);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
			return ResponseEntity.status(HttpStatus.OK).body(jsonResponse);
		}
	}

	/**
	 * Controller method for delete the event
	 * 
	 * @param eventId
	 * @return
	 */
	@DeleteMapping("/deleteevent/{eventId}")
	public ResponseEntity<?> deleteEvent(@PathVariable("eventId") int eventId) {
		boolean isDeleted = eventService.deleteEvent(eventId);
		if (isDeleted) {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Event Deleted\",\"statuscode\":" + 200 + "}");
		} else {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"No such event\",\"statuscode\":" + 201 + "}");
		}
	}

	/**
	 * This a controller method for search based on type of sport
	 * @param sportType
	 * @return if required sport type found returns list of all spsecified evetns, else empty list
	 */
	@GetMapping("/serachbysporttype/{sportType}")
	public ResponseEntity<?> serachBySportType(@PathVariable("sportType") String sportType) {
		List<Event> eventList = eventService.serachBySportType(sportType);

		String josnObject = null;
		if (eventList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"No Events Found\",\"statuscode\":" + 201 + "}");
		} else {
			try {
				josnObject = mapper.writeValueAsString(eventList);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"List of events\",\"statuscode\":" + 200 + ",\"\":" + josnObject + "}");
		}

	}

}
