package com.sports360.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sports360.entities.Team;
import com.sports360.entities.TeamModel;
import com.sports360.services.TeamService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/team")
public class TeamController {

	@Autowired
	private TeamService teamService;

	@Autowired
	private ObjectMapper mapper;
	
	
	@PostMapping("/creation")
	public ResponseEntity<?> teamCreation(@RequestBody Team team) {
		TeamModel teamObj = teamService.createTeam(team);
		String teamObject = null;
		try {
			teamObject = mapper.writeValueAsString(teamObj);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (teamObj!=null) {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"team Created successfully\",\"statuscode\":" + 200 + ",\"team\":"+teamObject+"}");
		} else {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Team Creation failed\",\"statuscode\":" + 201 + "}");
		}
	}
	
	@PostMapping("/addmembertoteam/{teamId}/{userId}")
	public ResponseEntity<?> addMemberToTeam(@PathVariable("teamId")int teamId,@PathVariable("userId")int userId ){
		TeamModel team = teamService.addMemberToTeam(teamId, userId);
		String teamObject = null;
		try {
			teamObject = mapper.writeValueAsString(team);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(team!=null) {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"member added successfully\",\"statuscode\":" + 200 + ",\"team\":"+teamObject+"}");
		}else {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"member adding failed\",\"statuscode\":" + 201 + "}");
		}
	}
	
	@GetMapping("/displayteam/{teamId}")
	public ResponseEntity<?> displayTeam(@PathVariable("teamId")int teamId){
		Team team = teamService.getTeamDetails(teamId);
		String teamObject = null;
		try {
			teamObject = mapper.writeValueAsString(team);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(team!=null) {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"member added successfully\",\"statuscode\":" + 200 + ",\"team\":"+teamObject+"}");
		}else {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"member adding failed\",\"statuscode\":" + 201 + "}");
		}
	}
}
