package com.sports360.entities;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class EventModel {

	private int eventId;
	private String eventName;
	private String sportType;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+5:30")
	private Date eventDate;
	private String location;
	private String subject;
	private String description;
	private int team_size;
	private int no_of_teams;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+5:30")
	private Date registrationFromDate;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+5:30")
	private Date registrationToDate;
	private double entryFee;
	private double winningPrize;

	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getSportType() {
		return sportType;
	}

	public void setSportType(String sportType) {
		this.sportType = sportType;
	}

	public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getTeam_size() {
		return team_size;
	}

	public void setTeam_size(int team_size) {
		this.team_size = team_size;
	}

	public int getNo_of_teams() {
		return no_of_teams;
	}

	public void setNo_of_teams(int no_of_teams) {
		this.no_of_teams = no_of_teams;
	}

	public Date getRegistrationFromDate() {
		return registrationFromDate;
	}

	public void setRegistrationFromDate(Date registrationFromDate) {
		this.registrationFromDate = registrationFromDate;
	}

	public Date getRegistrationToDate() {
		return registrationToDate;
	}

	public void setRegistrationToDate(Date registrationToDate) {
		this.registrationToDate = registrationToDate;
	}

	public double getEntryFee() {
		return entryFee;
	}

	public void setEntryFee(double entryFee) {
		this.entryFee = entryFee;
	}

	public double getWinningPrize() {
		return winningPrize;
	}

	public void setWinningPrize(double winningPrize) {
		this.winningPrize = winningPrize;
	}

}
