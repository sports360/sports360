package com.sports360.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="admin_tbl")
public class Admin {
	@Id
	@GenericGenerator(name = "IdGen", strategy = "com.sports360.util.IdGenerator")
	@GeneratedValue(generator = "IdGen")
	@Column(name ="admin_id")
	public int adminId;
	@Column
	public String userName;
	@Column
	public String emailId;
	@Column
	public String password;
	
	public int getAdminId() {
		return adminId;
	}
	public void setAdminId(int adminId) {
		this.adminId = adminId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	
	
}
