package com.sports360.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "user_tbl")
public class User {

	@Id
	@GenericGenerator(name = "IdGen", strategy = "com.sports360.util.IdGenerator")
	@GeneratedValue(generator = "IdGen")
	@Column(name = "user_id")
	public int userId;

	@Column
	public String userName;
	@Column
	public String password;
	@Column
	public String phoneNo;
	@Column
	public String emailId;
	@Column(name = "role",insertable=false)
	private String role;
//	@ManyToOne
//	@JoinColumn(name = "team_id", nullable = true)
//	private Team team;
	@ManyToMany(mappedBy="users")
	private Set<Team> teams =new HashSet<>();
	
	public Set<Team> getTeams() {
		return teams;
	}
	public void setTeams(Set<Team> teams) {
		this.teams = teams;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}

	

//	public Team getTeam() {
//		return team;
//	}
//
//	public void setTeam(Team team) {
//		this.team = team;
//	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

}
