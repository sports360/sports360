package com.sports360.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;


@Entity
@Table(name = "team_tbl")
public class Team {

	@Id
	@GenericGenerator(name = "IdGen", strategy = "com.sports360.util.IdGenerator")
	@GeneratedValue(generator = "IdGen")
	@Column(name = "team_id")
	private int teamId;
	@Column
	private String teamName;

	@ManyToOne
	@JoinColumn(name = "event_id")
	private Event event;
//	@Column
//	public int teamSize;

//	@Column
//	@OneToMany(mappedBy="team",cascade=CascadeType.ALL)
//	private List<User> users;

	@ManyToMany(cascade = CascadeType.MERGE)
	private Set<User> users = new HashSet<>();
// -------------------------------------	
	public void addUser(User user) {
        this.users.add(user);
        user.getTeams().add(this);
    }
 
    public void removeUser(User user) {
        this.users.remove(user);
        user.getTeams().remove(this);
    }

//------------------------------------
    
	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	@OneToOne
	private ScheduleEvent schedule;

	public int getTeamId() {
		return teamId;
	}

	public void setTeamId(int teamId) {
		this.teamId = teamId;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public ScheduleEvent getSchedule() {
		return schedule;
	}

	public void setSchedule(ScheduleEvent schedule) {
		this.schedule = schedule;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

//	public List<User> getUsers() {
//		return users;
//	}
//	public void setUsers(List<User> users) {
//		this.users = users;
//	}

//	public int getTeamSize() {
//		return teamSize;
//	}
//	public void setTeamSize(int teamSize) {
//		this.teamSize = teamSize;
//	}

}

