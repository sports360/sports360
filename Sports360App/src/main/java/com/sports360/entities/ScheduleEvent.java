package com.sports360.entities;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="scheduleEvent_tbl")
public class ScheduleEvent {
	@Id
	@GenericGenerator(name = "IdGen", strategy = "com.sports360.util.IdGenerator")
	@GeneratedValue(generator = "IdGen")
	@Column(name ="schedule_id")
	private int schedule_id;
	@Column
	private Date schedule_date;
	
	@OneToOne(mappedBy="schedule")
	@JoinColumn(name="team1Id")
	private Team team1;
	
	@ManyToOne
	@JoinColumn(name="eventId")
	private Event event;
	
	@OneToOne(mappedBy="schedule")
	@JoinColumn(name="team2Id")
	private Team team2;

	public int getSchedule_id() {
		return schedule_id;
	}

	public void setSchedule_id(int schedule_id) {
		this.schedule_id = schedule_id;
	}

	public Date getSchedule_date() {
		return schedule_date;
	}

	public void setSchedule_date(Date schedule_date) {
		this.schedule_date = schedule_date;
	}

}
