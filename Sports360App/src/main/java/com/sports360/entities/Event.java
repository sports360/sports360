package com.sports360.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "events_tbl")
public class Event {

	@Id
	@GenericGenerator(name = "IdGen", strategy = "com.sports360.util.IdGenerator")
	@GeneratedValue(generator = "IdGen")
	@Column(name = "event_id")
	private int eventId;
	@Column
	private String eventName;
	@Column
	private String sportType;
	@Column
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+5:30")
	private Date eventDate;
	@Column
	private String location;
	@Column
	private String subject;
	@Column
	private String description;
	@Column
	private int team_size;
	@Column
	private int no_of_teams;
	@Column
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+5:30")
	private Date registrationFromDate;
	@Column
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+5:30")
	private Date registrationToDate;
	@Column
	private double entryFee;
	@Column
	private double winningPrize;
	
	//TODO is it necessary
	@OneToMany(mappedBy = "event")
	private List<Team> registeredTeams;

	@OneToMany(mappedBy = "event")
	private List<ScheduleEvent> scheduleEvent;
	@OneToOne(mappedBy = "event")
	private Team winningTeam1;

	@OneToOne(mappedBy = "event")
	private Team winningTeam2;

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public int getTeam_size() {
		return team_size;
	}

	public void setTeam_size(int team_size) {
		this.team_size = team_size;
	}

//	@ManyToMany(targetEntity = Team.class, cascade = { CascadeType.ALL })
//	@JoinTable(name = "teams_events_details", joinColumns = { @JoinColumn(name = "eventId") }, inverseJoinColumns = {
//			@JoinColumn(name = "teamId") })

	public List<ScheduleEvent> getScheduleEvent() {
		return scheduleEvent;
	}

	public void setScheduleEvent(List<ScheduleEvent> scheduleEvent) {
		this.scheduleEvent = scheduleEvent;
	}

	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getSportType() {
		return sportType;
	}

	public void setSportType(String sportType) {
		this.sportType = sportType;
	}

	public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getNo_of_teams() {
		return no_of_teams;
	}

	public void setNo_of_teams(int no_of_teams) {
		this.no_of_teams = no_of_teams;
	}

	public Date getRegistrationFromDate() {
		return registrationFromDate;
	}

	public void setRegistrationFromDate(Date registrationFromDate) {
		this.registrationFromDate = registrationFromDate;
	}

	public Date getRegistrationToDate() {
		return registrationToDate;
	}

	public void setRegistrationToDate(Date registrationToDate) {
		this.registrationToDate = registrationToDate;
	}

	public double getEntryFee() {
		return entryFee;
	}

	public void setEntryFee(double entryFee) {
		this.entryFee = entryFee;
	}

	public double getWinningPrize() {
		return winningPrize;
	}

	public void setWinningPrize(double winningPrize) {
		this.winningPrize = winningPrize;
	}

	public List<Team> getRegisteredTeams() {
		return registeredTeams;
	}

	public void setRegisteredTeams(List<Team> registeredTeams) {
		this.registeredTeams = registeredTeams;
	}

	public Team getWinningTeam1() {
		return winningTeam1;
	}

	public void setWinningTeam1(Team winningTeam1) {
		this.winningTeam1 = winningTeam1;
	}

	public Team getWinningTeam2() {
		return winningTeam2;
	}

	public void setWinningTeam2(Team winningTeam2) {
		this.winningTeam2 = winningTeam2;
	}

}
