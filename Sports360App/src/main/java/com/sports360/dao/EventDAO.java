package com.sports360.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.sports360.entities.Event;

/**
 * Repository class for Event entity it will perform all the DAO operations
 * 
 * @author IMVIZAG
 *
 */
public interface EventDAO extends CrudRepository<Event, Integer> {

	List<Event> findAllByEventDate(Date eventDate);

	/**
	 * Dao Method for search the evetns by using location names
	 * 
	 * @param location
	 * @return list of events
	 */
	List<Event> findByLocation(String location);
	
	/**
	 * DAO method for search the sports by its type
	 * @param sportType
	 * @return list of evetns, if specified sport type is found, else retruns empty list
	 */
	
	List<Event> findBySportType(String sportType);

}
