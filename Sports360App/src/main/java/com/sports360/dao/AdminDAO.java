package com.sports360.dao;

import org.springframework.data.repository.CrudRepository;

import com.sports360.entities.Admin;

public interface AdminDAO extends CrudRepository<Admin, Integer> {

}
