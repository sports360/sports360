package com.sports360.dao;

import org.springframework.data.repository.CrudRepository;

import com.sports360.entities.Team;

public interface TeamDAO extends CrudRepository<Team, Integer>{

}
