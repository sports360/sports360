package com.sports360.dao;

import org.springframework.data.repository.CrudRepository;

import com.sports360.entities.User;

public interface UserDAO extends CrudRepository<User, Integer> {
	/**
	 * Repository method for find the emailId exists or not
	 * 
	 * @param emailId
	 * @return if emailId exists it will return User object
	 */
	public User findByemailId(String emailId);

}
