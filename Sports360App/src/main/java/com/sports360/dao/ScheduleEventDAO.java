package com.sports360.dao;

import org.springframework.data.repository.CrudRepository;


import com.sports360.entities.ScheduleEvent;

public interface ScheduleEventDAO extends CrudRepository<ScheduleEvent, Integer> {

}
