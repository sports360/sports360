package com.sports360;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Sports360AppApplication {

	public static void main(String[] args) {
		SpringApplication.run(Sports360AppApplication.class, args);
	}

}
