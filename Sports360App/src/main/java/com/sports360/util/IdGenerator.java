package com.sports360.util;



import java.io.Serializable;

import java.sql.Connection;
import java.sql.ResultSet;

import java.sql.Statement;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

/**
 * this class is used to create a auto generated values for the all auto increment variables
 * @author IMVIZAG
 *
 */

public class IdGenerator implements IdentifierGenerator {
	
	@Override
	public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {
		
		Connection con = session.connection();
		Integer newId = 0;
		ResultSet rs = null;
		Statement st = null;
		int prefix = 0;
		try {
			st = con.createStatement();
			switch (object.getClass().getName()) {
			case "com.sports360.entities.Admin":
				prefix = 1001;
				rs = st.executeQuery("select max(admin_id) as max from admin_tbl");

				break;
				
			case "com.sports360.entities.User":
				prefix = 2001;
				rs = st.executeQuery("select max(user_id) as max from user_tbl");
				break;
				
			case "com.sports360.entities.Team":
				prefix = 6001;
				rs = st.executeQuery("select max(team_id) as max from team_tbl");
				break;
				
			case "com.sports360.entities.Event":
				session.flush();
				prefix = 5001;
				rs = st.executeQuery("select max(event_id) as max from events_tbl");
				break;
				
			case "com.way2walkins.entities.ScheduleEvent":
				prefix = 4001;
				rs = st.executeQuery("select max(scheduleEvent_tbl) as max from schedule_id");
				break;
			
			default:
				break;
			}

			if (rs.next()) {
				newId = rs.getInt("max") == 0 ? rs.getInt("max") + prefix : rs.getInt("max") + 1;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return newId;
	}

}
