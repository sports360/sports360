package com.sports360.util;

public enum RoleEnum {
	
	ADMIN(1),USER(2);
	
	private int value;

	public int getValue() {
		return value;
	}

	private RoleEnum(int value) {
       this.value = value;
	}

}
