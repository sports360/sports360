package com.sports360.util;


import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class ModelMapperUtil {
 
 // @Autowired
  private ModelMapper modelMapper;
  
  public  <T,R> R convertEntityToDto(T entity,Class<R> type){
	  return modelMapper.map(entity, type);
  }
  
  
}