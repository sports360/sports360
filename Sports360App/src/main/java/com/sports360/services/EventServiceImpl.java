package com.sports360.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sports360.dao.EventDAO;
import com.sports360.entities.Event;
import com.sports360.entities.EventModel;

/**
 * implementation class for EventService
 * 
 * @author IMVIZAG
 *
 */
@Service
@Transactional
public class EventServiceImpl implements EventService {
	@Autowired
	private EventDAO eventDao;

	ModelMapper modelMapper = new ModelMapper();

	/**
	 * service method for creating a event
	 * 
	 * @return true if creation success, else false
	 */
	@Override
	public boolean eventCreation(Event event) {
		return eventDao.save(event) != null ? true : false;
	}

	@Override
	public List<EventModel> getAllEvents() {
		List<Event> eventList = (List<Event>) eventDao.findAll();
		List<EventModel> modelList = new ArrayList<>();
		for (Event event : eventList) {
			modelList.add(modelMapper.map(event, EventModel.class));
		}
		return modelList;
	}

	@Override
	public List<EventModel> getAllEventsOnDate(Date date) {
		List<Event> eventList =eventDao.findAllByEventDate(date);
		List<EventModel> modelList = new ArrayList<>();
		for (Event event : eventList) {
			modelList.add(modelMapper.map(event, EventModel.class));
		}
		return modelList;
	}

	/**
	 * service method for search the event by location
	 * 
	 * @param location
	 * @return list of events
	 */
	@Override
	public List<EventModel> searchEventByLocation(String location) {
		List<Event> eventList = eventDao.findByLocation(location);
		List<EventModel> modelList = new ArrayList<>();
		for (Event event : eventList) {
			modelList.add(modelMapper.map(event, EventModel.class));
		}
		return modelList;
	}

	@Override
	public boolean doModifyEvent(Event event) {

		return eventDao.save(event) != null ? true : false;
	}

	/**
	 * This method is used to fetch particular event based on eventId.
	 */
	@Override
	public Event getEventById(int eventId) {
		Optional<Event> event = eventDao.findById(eventId);
		return event.isPresent() ? event.get() : null;

	}

	/**
	 * Service method for delete the Event
	 * 
	 * @param eventId
	 * @return true, if event is deleted, else return false
	 */
	@Override
	public boolean deleteEvent(int eventId) {
		boolean status = false;
		Event event = new Event();
		event.setEventId(eventId);
		if (eventDao.existsById(eventId)) {
			eventDao.delete(event);
			status = true;
		}
		return status;
	}

	/**
	 * Service method for serach the particular sport based on its type
	 * 
	 * @param sportType
	 * @return list of events
	 */
	@Override
	public List<Event> serachBySportType(String sportType) {
		return eventDao.findBySportType(sportType);

	}

}
