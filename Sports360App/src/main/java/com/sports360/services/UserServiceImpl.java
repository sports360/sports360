package com.sports360.services;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sports360.dao.UserDAO;
import com.sports360.entities.LoginModel;
import com.sports360.entities.User;
import com.sports360.entities.UserModel;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserDAO userDao;
	
	private ModelMapper modelMapper = new ModelMapper();

	/**
	 * This method is used to save newly registered user object -1 if email already
	 * exist -2 if registration success -3 if registration failed
	 */
	@Override
	public int doUserRegister(User user) {

		if (userDao.findByemailId(user.getEmailId()) != null) {
			return -1;
		}
		return userDao.save(user) != null ? -2 : -3;
	}

	/**
	 * This is a service method for login the user
	 * 
	 * @return the status of login whether it is success or failure
	 */
	@Override
	public User doLogin(LoginModel loginModel) {
		User user = userDao.findByemailId(loginModel.getEmailId());
		if (user != null && user.getPassword().equals(loginModel.getPassword())) {
			return user;
		} else {
			return null;
		}
	}

	/**
	 * Service method for find the emailId
	 * 
	 * @param emailId
	 * @return user object if emailId found, else returns null;
	 */
	@Override
	public UserModel searchByEmailId(String emailId) {
		User user =userDao.findByemailId(emailId);
		
		return modelMapper.map(user, UserModel.class);

	}

}
