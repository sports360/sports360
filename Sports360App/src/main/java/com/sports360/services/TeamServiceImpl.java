package com.sports360.services;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sports360.dao.EventDAO;
import com.sports360.dao.TeamDAO;
import com.sports360.dao.UserDAO;
import com.sports360.entities.Event;
import com.sports360.entities.Team;
import com.sports360.entities.TeamModel;
import com.sports360.entities.User;
import com.sports360.entities.UserModel;

@Service
@Transactional
public class TeamServiceImpl implements TeamService {

	@Autowired
	private TeamDAO teamDao;
	@Autowired
	private EventDAO eventDao;
	@Autowired
	private UserDAO userDao;
	//@Autowired
	private ModelMapper modelmapper = new ModelMapper();

	@Override
	public TeamModel createTeam(Team team) {
//		Optional<Event> event = eventDao.findById(team.getEvent().getEventId());
//		if (event.isPresent()) {
//			team.setEvent(event.get());
//			Team teamObj = teamDao.save(team);
//			for(User user:team.getUsers()) {
//				teamObj.addUser(user);
//			}
			Team resultTeam = teamDao.save(team);
			Set<UserModel> modelusers = new HashSet<>();
			for(User member:resultTeam.getUsers()) {
				 modelusers.add(modelmapper.map(member, UserModel.class));
			}
			TeamModel modelTeam = modelmapper.map(resultTeam, TeamModel.class);
			modelTeam.setUsers(modelusers);
			return modelTeam;
//		} else {
//			return null;
//		}
	}
	
	
	@Override
	public TeamModel addMemberToTeam(int teamId,int userId) {
		Team team =teamDao.findById(teamId).get();
		User user = userDao.findById(userId).get();
		team.addUser(user);
		Team resultTeam = teamDao.save(team);
		Set<UserModel> modelusers = new HashSet<>();
		for(User member:resultTeam.getUsers()) {
			 modelusers.add(modelmapper.map(member, UserModel.class));
		}
		TeamModel modelTeam = modelmapper.map(resultTeam, TeamModel.class);
		modelTeam.setUsers(modelusers);
		return modelTeam;
	}


	@Override
	public Team getTeamDetails(int teamId) {
		return teamDao.findById(teamId).get();
	}

}
