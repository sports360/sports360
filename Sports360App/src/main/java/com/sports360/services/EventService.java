package com.sports360.services;

import java.util.Date;
import java.util.List;

import com.sports360.entities.Event;
import com.sports360.entities.EventModel;

public interface EventService {

	public boolean eventCreation(Event event);

	List<EventModel> getAllEvents();

	List<EventModel> searchEventByLocation(String location);

	List<EventModel> getAllEventsOnDate(Date date);

	boolean doModifyEvent(Event event);

	Event getEventById(int eventId);

	public boolean deleteEvent(int eventId);
	
	public List<Event> serachBySportType(String sportType);
}
