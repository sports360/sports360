package com.sports360.services;

import com.sports360.entities.LoginModel;
import com.sports360.util.UserBasicDto;

public interface AdminService {
	
	public UserBasicDto adminLogin(LoginModel loginModel) throws Exception;

}
