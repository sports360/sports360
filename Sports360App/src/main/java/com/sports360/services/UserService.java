package com.sports360.services;

import com.sports360.entities.LoginModel;
import com.sports360.entities.User;
import com.sports360.entities.UserModel;

public interface UserService {
	
	int doUserRegister(User user);
	
	User doLogin(LoginModel loginModel);
	
	UserModel searchByEmailId(String emailId);

}
