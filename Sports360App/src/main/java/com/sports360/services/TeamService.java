package com.sports360.services;

import com.sports360.entities.Team;
import com.sports360.entities.TeamModel;

public interface TeamService {
	TeamModel createTeam(Team team);
	TeamModel addMemberToTeam(int teamId,int userId);
	Team getTeamDetails(int teamId);
}
