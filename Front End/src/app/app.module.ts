import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from './header/nav/nav.component';
import { FooterComponent } from './header/footer/footer.component';
import { AuthModule } from './auth/auth.module';
import { AdminModule } from './admin/admin.module';
import { UserModule } from './user/user.module';
import { SubscribeComponent } from './header/subscribe/subscribe.component';
import { ContactComponent } from './contact/contact.component';
import { TestmonialComponent } from './header/testmonial/testmonial.component';

// import {MyDatePickerModule} from 'mydatepicker';


@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    FooterComponent,
    SubscribeComponent,
    ContactComponent,
    TestmonialComponent,    

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AuthModule,
    AdminModule,
    UserModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
