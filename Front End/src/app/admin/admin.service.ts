import { Injectable }                 from '@angular/core';
import { HttpClient }                 from '@angular/common/http';
import { BehaviorSubject }            from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private http: HttpClient) {}

  getEvents() {
    return this.http.get<any>('/sports360app/event/getallevents');
  }

  doCreateEvent(data: any) {
    return this.http.post<any>('/sports360app/event/creation', data);
  }
  doGetEvemtById(id: any) {
    return this.http.get<any>('/sports360app/event/geteventbyid/' + id);
  }
  doDelete(id: any) {
    return this.http.delete<any>('/sports360app/event/deleteevent/' + id);
  }
  doUpdateEvent(data : any){
    return this.http.put<any>('/sports360app/event/modifyevent',data);
  }
}
