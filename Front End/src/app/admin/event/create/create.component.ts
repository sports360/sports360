
// component based imports
import { Component, OnInit ,ViewChild} from '@angular/core';
// form based imports
import { NgForm } from '@angular/forms';
// service based imports
import { AdminService } from '../../admin.service';
// routing baseed imports
import {Router} from '@angular/router';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  @ViewChild('f') createEvent:NgForm;
  minDate: Date;
  eventDate:Date;
  eventminDate:Date;
  maxFromRegistrationDate:Date;
  minToRegistrationDate:Date;
  // maxDate: Date;
  constructor(private service:AdminService,private router: Router,) { }
  ngOnInit() {
    this.minDate = new Date();  
    this.minDate.setDate(this.minDate.getDate());

   
  }
  valuechange(searchValue : Date ) {  
    let maxDate=searchValue.getDate();
    this.maxFromRegistrationDate=new Date();
    this.maxFromRegistrationDate.setDate(maxDate);
console.log( searchValue.getDate());}

registrtionmin(searchValue : Date){
  let minDate=searchValue.getDate();    
  this.minToRegistrationDate=new Date();
  this.minToRegistrationDate.setDate(minDate);
}

  onSubmit()
  {
    console.log(this.createEvent.value)
    this.service.doCreateEvent(this.createEvent.value).subscribe(
      (data)=>{
        (data.statuscode == 200)
        {
          this.router.navigate(['admin'])
        }
        console.log(data)
      }
    )
  }

}
