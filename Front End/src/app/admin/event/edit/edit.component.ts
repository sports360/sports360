import { Component, OnInit,ViewChild } from '@angular/core';
import { AdminService } from '../../admin.service';
// form based imports
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router'
import { AnonymousSubject } from 'rxjs/internal/Subject';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  constructor(private service:AdminService,private activatedRoute: ActivatedRoute) { }
  event:any;
  eventId:any;
  eventData:any;
  minDate: Date;
  eventDate:Date;
  eventminDate:Date;
  maxFromRegistrationDate:Date;
  minToRegistrationDate:Date;
  @ViewChild('f') updateEvent:NgForm;
  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      let id = params['id'];
      console.log(id);
      this.service.doGetEvemtById(id).subscribe(
        (data)=>{
          this.event=data.result;
          this.eventId=data.result.eventId;
          console.log(this.event);
          
          this.minDate = new Date();  
          this.minDate.setDate(this.minDate.getDate());
        }
      )
  
  });
  }
   
  onSubmit(){
    this.eventData=this.updateEvent.value
    this.eventData.eventId=this.eventId;
    this.service.doUpdateEvent(this.eventData).subscribe(data =>{
      console.log(data);
    })
        console.log(this.updateEvent.value);
  }

  valuechange(searchValue : Date ) {  
    let maxDate=searchValue.getDate();
    this.maxFromRegistrationDate=new Date();
    this.maxFromRegistrationDate.setDate(maxDate);
    console.log( searchValue.getDate());}

registrtionmin(searchValue : Date){
  let minDate=searchValue.getDate();    
  this.minToRegistrationDate=new Date();
  this.minToRegistrationDate.setDate(minDate);
}

}
