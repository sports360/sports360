import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule }      from '@angular/forms';
import { HttpClientModule }                      from '@angular/common/http';

import {BsDatepickerModule} from 'ngx-bootstrap/datepicker';
import { AdminComponent } from './admin.component';
import { CreateComponent } from './event/create/create.component';
import { EditComponent } from './event/edit/edit.component';
import { DeleteComponent } from './event/delete/delete.component';
import { ReadComponent } from './event/read/read.component';
import { SheduleComponent } from './shedule/shedule.component';
import { AdminRoutingModule } from './admin-route.module';
@NgModule({
  declarations: [
    
    AdminComponent,
    CreateComponent,
    EditComponent,
    DeleteComponent,
    ReadComponent,
    SheduleComponent,
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BsDatepickerModule.forRoot()
  ]
})
export class AdminModule { }
