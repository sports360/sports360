import { Component, OnInit } from '@angular/core';
import { AdminService } from './admin.service';
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  constructor(private service:AdminService) { }
  
  events:any;

  ngOnInit() {
    this.service.getEvents().subscribe(
      (data)=>{
        this.events=data.eventlist;
        console.log(this.events);
      }
    )
  }

  delete(id : any){
    alert("Delete Event ");
    this.service.doDelete(id).subscribe(
      (data)=>{
        this.events=data.eventlist;
        console.log(this.events);
        window.location.href="/admin";
      }
    )
    // perform your action
    }

}
