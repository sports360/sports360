import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateComponent } from '../admin/event/create/create.component';
import { DeleteComponent } from '../admin/event/delete/delete.component';
import { EditComponent } from '../admin/event/edit/edit.component';
import { ReadComponent } from '../admin/event/read/read.component';
import { SheduleComponent } from './shedule/shedule.component';
import { AdminComponent } from './admin.component';


const routes: Routes = [
  {path: 'create', component:CreateComponent},
  {path: 'delete', component:DeleteComponent},
  {path: 'edit', component:EditComponent},
  {path:'read', component:ReadComponent},
  {path:'schedule', component:SheduleComponent},
  {path:'admin', component:AdminComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
