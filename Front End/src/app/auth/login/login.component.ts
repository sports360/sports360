 /*
  * Author      : Ramu
    Date        : 1-04-2019
    Description : login class to validate the credentials and redirect to dashboar after successful login
  */

// component based imports
import { Component, OnInit } from '@angular/core';
// form based imports
import { FormControl , FormGroup,Validators} from '@angular/forms';
// service based imports
import { AuthService } from '../auth.service';
// routing baseed imports
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

 loginform:FormGroup;
  passwordStatus:number;
  username:string;
  pswd:string;

  constructor(private service:AuthService,private router: Router,) { }

  ngOnInit() {
    if(localStorage.getItem('userId'))
    {
      this.router.navigate(['']);
    }
    this.loginform=new FormGroup({
      emailId: new FormControl('',[Validators.required, Validators.email]),
      password: new FormControl('',Validators.required),
    });
    console.log(this.loginform)
  }

  onSubmit()
  {
    console.log(this.loginform.value);

    this.service.doLogin(this.loginform.value).subscribe(
      (data)=>{
        if(data.statuscode == 200)
        {
          localStorage.setItem('userId',data.userId);
          localStorage.setItem('role',data.role);
          this.service.checkNavLogin(true);
            
          // this.service.headerNavbar();
          // this.service.loginButton();
          if(data.role=="user")
          {
            this.router.navigate([''])
          }
          if(data.role=="admin")
          {
            this.router.navigate(['admin'])
          }
          
          // localStorage.setItem('auth_time',Date.now());
          
        }
        else{
          if(data.statuscode==-1)
          {
            this.username="";
            this.pswd="";
            this.loginform.value.password="";
            alert("no such username found")
          }
          else if(data.statuscode<-1)
          {
            this.pswd="";
          }
          this.passwordStatus=data.statuscode;
        }
      }
      
    )
   
  }
  
}
