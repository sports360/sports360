import { Injectable }                 from '@angular/core';
import { HttpClient }                 from '@angular/common/http';
import { BehaviorSubject }            from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private user=new BehaviorSubject<boolean>(false);

  isLogin=this.user.asObservable();

  constructor(private http: HttpClient) {}

  checkNavLogin(islogin:boolean)
  {
    this.user.next(islogin);
  }

  doLogin(data: any) {
    return this.http.post<any>('/sports360app/user/login', data);
  }
  dosignup(data: any) {
    return this.http.post<any>('/sports360app/user/registration', data);
  }
}
