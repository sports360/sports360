import { Component, OnInit } from '@angular/core';
// form based imports
import { FormControl , FormGroup,Validators} from '@angular/forms';
// service based imports
import { AuthService } from '../auth.service';
// routing baseed imports
import {Router} from '@angular/router';
@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  signupform:FormGroup;
  passwordStatus:number;
  username:string;
  pswd:string;
  constructor(private service:AuthService,private router: Router,) { }

  ngOnInit() {
    if(localStorage.getItem('userId'))
    {
      this.router.navigate(['']);
    }
    this.signupform=new FormGroup({
      emailId: new FormControl('',[Validators.required, Validators.email]),
      password: new FormControl('',Validators.required),
      phoneNo: new FormControl('',Validators.required),
      userName: new FormControl('',Validators.required),
    });
    console.log(this.signupform)
  }

  onSubmit()
  {
    console.log(this.signupform.value);

    this.service.dosignup(this.signupform.value).subscribe(
      (data)=>{
        if(data.statuscode == 200)
        {
          this.router.navigate(['/login'])          
          
          // localStorage.setItem('auth_time',Date.now());
          
        }
        else{
          if(data.statuscode==-1)
          {
            this.username="";
            this.pswd="";
            this.signupform.value.password="";
            alert("no such username found")
          }
          else if(data.statuscode<-1)
          {
            this.pswd="";
          }
          this.passwordStatus=data.statuscode;
        }
      }
      
    )
   
  }

}
