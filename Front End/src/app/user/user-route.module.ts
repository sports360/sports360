import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from '../user/dashboard/dashboard.component';
import { HomeComponent } from '../user/home/home.component';
import { SelectteamComponent } from '../user/selectteam/selectteam.component';
import { SearchLocationComponent } from './search-location/search-location.component';
import { from } from 'rxjs';

const routes: Routes = [
  {path:'dashboard', component:DashboardComponent},
  {path:'', component:HomeComponent},
  {path:'selectteam', component:SelectteamComponent},
  {path:'location',component:SearchLocationComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }