import { Component, OnInit } from '@angular/core';
import {UserService} from '../user.service';
import { ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-search-location',
  templateUrl: './search-location.component.html',
  styleUrls: ['./search-location.component.css']
})
export class SearchLocationComponent implements OnInit {

  constructor(private service:UserService,private activatedRoute: ActivatedRoute) { }

  events:any;

  ngOnInit() {
 
    this.activatedRoute.queryParams.subscribe(params => {
      let data = params['search'];
      console.log(data);
      this.service.getLocation(data).subscribe(
        (data)=>{
          this.events=data.Events;
          console.log(this.events);
        }
      )
  
  });
}
   
  

}
