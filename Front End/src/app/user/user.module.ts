import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeComponent } from '../user/home/home.component';
import { SelectteamComponent } from '../user/selectteam/selectteam.component';
import { DashboardComponent } from '../user/dashboard/dashboard.component';
import { UserRoutingModule } from './user-route.module';
import { SearchLocationComponent } from './search-location/search-location.component';


@NgModule({
  declarations: [
    HomeComponent,
    SelectteamComponent,
    DashboardComponent,
    SearchLocationComponent
   
  ],
  imports: [
    CommonModule,
    UserRoutingModule
  ]
})
export class UserModule { }
