import { Injectable }                 from '@angular/core';
import { HttpClient }                 from '@angular/common/http';
import { BehaviorSubject }            from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) {}

  getEvents() {
    return this.http.get<any>('/sports360app/event/getallevents');
  }

  getLocation(search:string){
    console.log(search)
    return this.http.get<any>('/sports360app/event/searcheventbylocation/'+search);
  }
}
