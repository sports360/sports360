import { Component, OnInit } from '@angular/core';
import {UserService} from '../user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private service:UserService) { }

  events:any;

  ngOnInit() {
    this.service.getEvents().subscribe(
      (data)=>{
        this.events=data.eventlist;
        console.log(this.events);
      }
    )
  }

}
